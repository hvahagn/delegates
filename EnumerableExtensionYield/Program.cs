﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumerableExtensionYield
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = CreateRandomEnumerable(100).ToList();

            List<int> result = list.
                GreaterThan(55)
                .OddNumbers()
                .ToList();


            Print(result);
            Console.ReadLine();
        }

        public static IEnumerable<int> CreateRandomEnumerable(int count)
        {
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                yield return rnd.Next(1, 150);
            }
        }
        public static void Print(IEnumerable<int> source)
        {
            foreach (var item in source)
            {
                Console.WriteLine(item+" ");
            }
        }
    }
}
