﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_13
{
    static class MyClass
    {
        public static void Method()
        {
            Console.WriteLine("Строку вывел метод сообщенный с делегатом.");
        }
    }

    public delegate void MyDelegate();

    class Program
    {
        static void Main()
        {
            // MyDelegate myDelegate = new MyDelegate(MyClass.Method);

            MyDelegate myDelegate = MyClass.Method; // Предположение делегата.
            myDelegate();

            // Delay.
            Console.ReadKey();
        }
    }
}
