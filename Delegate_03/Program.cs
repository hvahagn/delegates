﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_03
{
    class MyClass
    {
        public string Method(string name)
        {
            return "Hello " + name+"!";
        }
    }
    public delegate string MyDelegate(string name);
    class Program
    {
        static void Main(string[] args)
        {
            MyClass instance = new MyClass();

            MyDelegate del = new MyDelegate(instance.Method);

            string user=del("Vahagn");
            Console.WriteLine(user);

            string greeting = del.Invoke("Armenia");
            Console.WriteLine(greeting);
            Console.ReadLine();
        }
    }
}
