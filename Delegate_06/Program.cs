﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_06
{
    public delegate int MyDelegate(int a, int b);
    class Program
    {
        static void Main(string[] args)
        {
            int sum1 = 1, sum2 = 2, sum = 0;

            MyDelegate myDel = delegate (int a, int b) { return a + b; };

            sum = myDel(sum1, sum2);

            Console.WriteLine("{0} + {1} = {2}",sum1, sum2, sum);


            Console.ReadLine();
        }
    }
}
