﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_08
{
    public delegate int MyDelegate(int a);
    class Program
    {
        static void Main(string[] args)
        {
            MyDelegate mydelegate;
            mydelegate = delegate (int x) { return x * 2; };  // Лямбда-Метод 
            mydelegate = (x) => { return x * 2; }; // Лямбда-Оператор.
            mydelegate = x => x * 2;   // Лямбда-Выражение.

           MyDelegate mydelegate2 = delegate (int x) { return x * 5; };
            mydelegate2 = (x) => { return x * 5; };
            mydelegate2 = x => x * 10;


            int result = mydelegate(4);
            Console.WriteLine(result);


            Console.ReadLine();
        }
    }
}
