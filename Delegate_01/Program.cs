﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_01
{
    static class MyClass
    {
        public static void Method()
        {
            Console.WriteLine("Hello delegate!!");
        }
        public static int Operation(int a, int b)
        {
            return a + b;
        }
    }
    
    public delegate void MyDelegate();
    public delegate int MyDelegateAddNumbers(int x, int y);


    class Program
    {
        static void Main(string[] args)
        {

            MyDelegate myDel = new MyDelegate(MyClass.Method);

          
            myDel();
            myDel.Invoke();

            MyDelegateAddNumbers del2 = new MyDelegateAddNumbers(MyClass.Operation);

            int result=del2.Invoke(10, 35);
            int result2 = del2(50, 77);
            Console.WriteLine(result+"-***- "+result2);

            Console.ReadLine();
        }
    }
}
