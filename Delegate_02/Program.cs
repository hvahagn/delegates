﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_02
{
    
    class MyClass
    {
        public void Method()
        {
            Console.WriteLine("Hello from delagate");
        }
    }

    public delegate void MyDelegate();

    class Program
    {
        static void Main(string[] args)
        {
            MyClass instance = new MyClass();
            MyDelegate del = new MyDelegate(instance.Method);
            del();
            del.Invoke();

            Console.ReadLine();
        }
    }

    



}
