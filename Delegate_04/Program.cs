﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_04
{
    public delegate void MyDelegate();
    class Program
    {
        public static void Method()
        {
            Console.WriteLine("Method 1");
        }
        public static void Method2()
        {
            Console.WriteLine("Method 2");
        }
        public static void Method3()
        {
            Console.WriteLine("Method 3");
        }
        static void Main(string[] args)
        {
            MyDelegate mydelegate = null;
            MyDelegate mydelegate1 = new MyDelegate(Method);
            MyDelegate mydelegate2 = new MyDelegate(Method2);
            MyDelegate mydelegate3 = new MyDelegate(Method3);
            mydelegate = mydelegate1 + mydelegate2 + mydelegate3;

            while (true)
            {
                Console.WriteLine("Please enter number from 0-7");
            string choice = Console.ReadLine();
           
                switch (choice)
                {
                    case "1":
                        mydelegate1.Invoke();
                        break;
                    case "2":
                        mydelegate2.Invoke();
                        break;
                    case "3":
                        mydelegate3.Invoke();
                        break;
                    case "4":
                        MyDelegate mydelegate4 = mydelegate - mydelegate1;
                        mydelegate4.Invoke();
                        break;
                    case "5":
                        MyDelegate mydelegate5 = mydelegate - mydelegate2;
                        mydelegate5.Invoke();
                        break;
                    case "6":
                        MyDelegate mydelegate6 = mydelegate - mydelegate3;
                        mydelegate6.Invoke();
                        break;
                    case "7":
                        mydelegate.Invoke();
                        break;
                    default:
                        Console.WriteLine("Вы ввели недопустимое значение.");
                        break;


                }

            }
            




            Console.ReadLine();
        }
    }
}
