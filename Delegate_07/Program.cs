﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_07
{
    public delegate void MyDelegate(ref int a, ref int b, out int c);
    class Program
    {
        static void Main(string[] args)
        {
            int sum1 = 1, sum2 = 2, sum = 0;

            MyDelegate mydelegate = delegate (ref int a, ref int b, out int c) { a++; b++; c = a + b; };

            mydelegate(ref sum1, ref sum2, out sum);

            Console.WriteLine("{0} + {1} = {2}",sum1,sum2,sum);
            Console.ReadLine();
        }
    }
}
