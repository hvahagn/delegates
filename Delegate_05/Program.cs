﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_05
{
    // Анонимные (лямбда) методы.
    public delegate void MyDelegate();
    class Program
    {
        static void Main(string[] args)
        {
            MyDelegate mydelegate = delegate { Console.WriteLine("Hello Delegate 777!"); };
            mydelegate();
            mydelegate.Invoke();

            Console.ReadLine();
        }
    }
}
